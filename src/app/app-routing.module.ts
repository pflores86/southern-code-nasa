import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MarsRoverComponent } from './components/mars-rover/mars-rover.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
  ,{
    path: 'home',
    component: HomeComponent
  }
  ,{
    path: 'mars-rover/:type',
    component: MarsRoverComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
