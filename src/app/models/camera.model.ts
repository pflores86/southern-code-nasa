export interface Camera {
  abbreviation: string,
  camera: string,
  rover: string[]
}
