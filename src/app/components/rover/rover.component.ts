import { Component, Input } from '@angular/core';

import { RoverPhoto } from '../../models/rover-photo.model';

@Component({
  selector: 'app-rover',
  templateUrl: './rover.component.html',
  styleUrls: ['./rover.component.scss']
})
export class RoverComponent {

  @Input() rover: RoverPhoto = {
    id: 0,
    sol: 0,
    camera: {
      id: 0,
      name: '',
      rover_id: 0,
      full_name: ''
    },
    img_src: '',
    earth_date: '',
    rover: {
      id: 0,
      name: '',
      landing_date: '',
      launch_date: '',
      status: ''
    }
  };

  constructor() { }
}
