import { Component, OnInit } from '@angular/core';
import { RoverPhoto } from '../../models/rover-photo.model';
import { Camera } from '../../models/camera.model';
import { RoverManifest, PhotoManifest } from '../../models/rover-manifest.model';
import { ActivatedRoute } from '@angular/router';
import { MarsRoverService } from '../../services/mars-rover.service';
import { CamerasService } from '../../services/cameras.service';
import { subDays } from 'date-fns'
import { FormControl, Validators,FormGroup,FormBuilder } from '@angular/forms'

const FILTER_PAG_REGEX = /[^0-9]/g;

@Component({
  selector: 'app-mars-rover',
  templateUrl: './mars-rover.component.html',
  styleUrls: ['./mars-rover.component.scss'],
})
export class MarsRoverComponent implements OnInit {
  form!: FormGroup;

  type: string | null = '';
  roverPhoto: RoverPhoto[] = [];
  cameras: Camera[] = [];
  roverManifest: RoverManifest = {
    name: '',
    landing_date: '',
    launch_date: '',
    status: '',
    max_sol: 0,
    max_date: '',
    total_photos: 0,
    photos: [],
  };
  page: number = 1;
  currentEarthDay: string = subDays(new Date(),1).toISOString().substr(0, 10);
  currentSol: number = 0;
  cameraFiltered: string = 'all';
  maxSolDate: number = 0;
  totalPhotos: number = 0;
  solDateStored: string | null = null;
  pageSize: number = 25;
  isSearchCollapse: boolean = true;



  constructor(
    private route: ActivatedRoute,
    private marsRoverService: MarsRoverService,
    private camerasService: CamerasService,
    private formBuilder: FormBuilder
  ) {

  }
  ngOnInit(): void {

    this.route.paramMap.subscribe((params) => {
      this.type = params.get('type');
      this.solDateStored = localStorage.getItem(`solDate${this.type}`);
      if (this.type) {
        //Fetch manifest info by Rover
        this.marsRoverService
          .getManifestsByRover(this.type)
          .subscribe((data) => {
            this.roverManifest = data.photo_manifest;
            let photo = this.roverManifest.photos.filter(ph => ph.sol == this.roverManifest.max_sol);
            this.totalPhotos = photo ? photo[0].total_photos : 0;
            this.currentSol = this.solDateStored ? Number(this.solDateStored) : this.roverManifest.max_sol;
            this.maxSolDate = this.roverManifest.max_sol;
            this.currentEarthDay = this.roverManifest.max_date;
            this.fetchCamerasByMartianSol(this.currentSol);
            this.fetchPhotosByMartianSol(this.currentSol);
            this.buildForm();
          });

      }
    });

  }
  private buildForm(){
    this.form = this.formBuilder.group({
      earthDaySearch : new FormControl (subDays(new Date(),1).toISOString().substr(0, 10), [Validators.required]),
      solDateSearch : new FormControl (this.maxSolDate, [Validators.required, Validators.max(this.maxSolDate), Validators.min(0)])
    })
  }
  //Fetch photos by MartianSol
  fetchPhotosByMartianSol(sol: number){
    this.marsRoverService
    .getAllPhotosByMartianSol(
      String(this.type),
      sol,
      this.cameraFiltered,
      this.page
    )
    .subscribe((data) => {
      this.roverPhoto = data.photos;
    });
  }
  //Filter data by Camera
  filterByCamera() {
    this.marsRoverService
      .getAllPhotosByMartianSol(
        String(this.type),
        this.currentSol,
        this.cameraFiltered,
        this.page
      )
      .subscribe((data) => {
        this.roverPhoto = data.photos
      });
  }
  //Reload cameras list to filter by the current Earth date/Martian sol
  fetchCamerasByMartianSol(sol : number){
    let photo : PhotoManifest[] = this.roverManifest.photos.filter(ph => ph.sol == sol);
    this.cameras = this.camerasService
          .getCameras()
          .filter((cam) => (photo.filter(photoCam => photoCam.cameras.includes(cam.abbreviation)).length > 0)) //.includes(String(this.type)));
  }
  //Link these fields
  linkEarthDateAndMartianSol(field: string){
    if (field==='earthDay'){
      let photo = this.roverManifest.photos.filter(ph => ph.earth_date == this.earthDayField?.value);
      this.form.setValue({solDateSearch: (photo.length > 0 ? photo[0].sol : ''),earthDaySearch: this.earthDayField?.value})
    }
    else if (field==='sol'){
      let photo = this.roverManifest.photos.filter(ph => ph.sol == this.solDateField?.value);
      console.log(photo);
      this.form.setValue({solDateSearch: this.solDateField?.value ,earthDaySearch: (photo.length > 0 ? photo[0].earth_date : -1)})
    }
  }
  //Search photos
  search(){
    if (this.form.valid){
      this.searchRoverPhotos();
    } else {
      this.form.markAllAsTouched();
    }
  }
  //Search photos by Earth and Sol
  searchRoverPhotos(){
    this.restoreValues();
    localStorage.setItem(`solDate${this.type}`, this.solDateField?.value);
    this.marsRoverService
          .getAllPhotosByMartianSol(
            String(this.type),
            this.solDateField?.value,
            this.cameraFiltered,
            this.page
          )
          .subscribe((data) => {
            this.roverPhoto = data.photos;
            this.currentSol = this.solDateField?.value
            this.currentEarthDay = this.earthDayField?.value
            this.fetchCamerasByMartianSol(this.solDateField?.value);
            this.isSearchCollapse = true;
          });
  }
  cancelSearch(){
    this.isSearchCollapse = true;
  }
  //Update page list by pagination usage
  selectPage(page: string) {
    this.page = parseInt(page, 10) || 1;
    this.fetchPhotosByMartianSol(this.currentSol);
  }

  //Method used by ngb-pagination
  formatInput(input: HTMLInputElement) {
    input.value = input.value.replace(FILTER_PAG_REGEX, '');
  }

  restoreValues(){
    this.cameraFiltered = 'all';
    this.page = 1;
  }

  //
  get earthDayField() {
    return this.form.get('earthDaySearch');
  }

  get isEarthDayFieldValid() {
    return this.earthDayField?.touched && this.earthDayField.valid;
  }

  get isEarthDayFieldInvalid() {
    return this.earthDayField?.touched && this.earthDayField.invalid;
  }

  get solDateField() {
    return this.form.get('solDateSearch');
  }

  get isSolDateFieldValid() {
    return this.solDateField?.touched && this.solDateField.valid;
  }

  get isSolDateFieldInvalid() {
    return this.solDateField?.touched && this.solDateField.invalid;
  }
}
