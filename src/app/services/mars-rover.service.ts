import { Injectable } from '@angular/core';
import { HttpClient,HttpParams } from '@angular/common/http';
import { Photos } from '../models/rover-photo.model';
import { RoverPhotoManifest } from '../models/rover-manifest.model';

const API_URL = "https://api.nasa.gov/mars-photos/api/v1";
const API_KEY = "KaFzhIo989LLxua7tCuiB5OtcGys4L2bQNkDtGJs";

@Injectable({
  providedIn: 'root',
})
export class MarsRoverService {
  constructor(private http: HttpClient) {}
  getManifestsByRover(
    rover: string
  ) {
    let params = new HttpParams();
    params = params.set('api_key', API_KEY);
    return this.http.get<RoverPhotoManifest>(`${API_URL}/manifests/${rover}`, { params });
  }
  getAllPhotosByMartianSol(
    rover: string,
    sol: number,
    camera: string,
    page: number
  ) {
    let params = new HttpParams();
    params = params.set('sol', sol);
    params = params.set('page', page);
    if (camera != 'all') {
      params = params.set('camera', camera);
    }
    params = params.set('api_key', API_KEY);
  return this.http.get<Photos>(`${API_URL}/rovers/${rover}/photos`, { params });
  }
  getAllPhotosByEarthdate(
    rover: string,
    earthDate: string,
    camera: string,
    page: number
  ) {
    let params = new HttpParams();
    params = params.set('earth_date', earthDate);
    params = params.set('page', page);
    if (camera != 'all') {
      params = params.set('camera', camera);
    }
    params = params.set('api_key', API_KEY);
    return this.http.get<Photos>(`${API_URL}/rovers/${rover}/photos`, { params });
  }
}
