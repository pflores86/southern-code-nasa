import { Injectable } from '@angular/core';
import { Camera } from '../models/camera.model';

@Injectable({
  providedIn: 'root'
})
export class CamerasService {

  cameras : Camera[] = [
    {
      abbreviation:'FHAZ',
      camera: 'Front Hazard Avoidance Camera',
      rover:	['curiosity','opportunity','spirit']
    },
    {
      abbreviation:'RHAZ',
      camera: 'Rear Hazard Avoidance Camera',
      rover:	['curiosity','opportunity','spirit']
    },
    {
      abbreviation:'MAST',
      camera: 'Mast Camera',
      rover:	['curiosity']
    },
    {
      abbreviation:'CHEMCAM',
      camera: 'Chemistry and Camera Complex',
      rover:	['curiosity']
    },
    {
      abbreviation:'MAHLI',
      camera: 'Mars Hand Lens Imager',
      rover:	['curiosity']
    },
    {
      abbreviation:'MARDI',
      camera: 'Mars Descent Imager',
      rover:	['curiosity']
    },
    {
      abbreviation:'NAVCAM',
      camera: 'Navigation Camera',
      rover:	['curiosity','opportunity','spirit']
    },
    {
      abbreviation:'PANCAM',
      camera: 'Panoramic Camera',
      rover:	['opportunity','spirit']
    },
    {
      abbreviation:'MINITES',
      camera: 'Miniature Thermal Emission Spectrometer (Mini-TES)',
      rover:	['opportunity','spirit']
    }
  ]

  constructor() { }

  public getCameras(){
    return this.cameras;
  }
}
